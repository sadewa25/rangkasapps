package com.example.sadewawicak.rangkasapps.BerandaActivity.adapterBeranda;

/**
 * Created by root on 2/28/18.
 */

public class modelBerandaItem {
    private String textTittle,textMessage;
    private int img;

    public String getTextTittle() {
        return textTittle;
    }

    public void setTextTittle(String textTittle) {
        this.textTittle = textTittle;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public modelBerandaItem(String textTittle, int img, String textMessage){
        this.img = img;
        this.textTittle = textTittle;
        this.textMessage = textMessage;
    }
}
