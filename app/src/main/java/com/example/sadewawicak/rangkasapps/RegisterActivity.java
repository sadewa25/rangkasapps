package com.example.sadewawicak.rangkasapps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.ContentValues.TAG;

public class RegisterActivity extends Activity {

    private FirebaseAuth mFirebaseAuth;

//    PhoneAuth
    private PhoneAuthProvider phoneAuthProvider;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private PhoneAuthProvider.ForceResendingToken mResendingToken;
    private String mVerificationID;

    @BindView(R.id.register_email)EditText registerEmail;
    @BindView(R.id.register_password)EditText registerPass;
    @BindView(R.id.register_konfirmasipass)EditText registerKonfirmasiPass;
    @BindView(R.id.register_nohandphone)EditText registerNoHandphone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mFirebaseAuth = FirebaseAuth.getInstance();

        ButterKnife.bind(this);

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                if (e instanceof FirebaseAuthInvalidCredentialsException){
                    registerNoHandphone.setError("Invalid Phone Number");
                }else if (e instanceof FirebaseTooManyRequestsException){
                    registerNoHandphone.setError("Too Many Request");
                }
            }
        };
    }

    @OnClick(R.id.register_button)void toRegisterUser(){
        if (validationEmpty()==true){
            if (registerPass.getText().toString().equals(registerKonfirmasiPass.getText().toString())){
//                startPhoneNumberVerification(registerNoHandphone.getText().toString());
                mFirebaseAuth.createUserWithEmailAndPassword(registerEmail.getText().toString(),
                        registerPass.getText().toString()).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            startActivity(new Intent(getApplicationContext(),MainActivity.class));
                            finish();
                        }else{
                            Toast.makeText(RegisterActivity.this, "GAGAL MENDAFTAR", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }else{
                alertDialog(getResources().getString(R.string.warning_tittle),
                        getResources().getString(R.string.warning_datapassword),RegisterActivity.this);
            }
        }
    }

    public boolean validationEmpty(){
        if (registerEmail.getText().toString().isEmpty()||
                registerNoHandphone.getText().toString().isEmpty()||
                registerPass.getText().toString().isEmpty()||
                registerKonfirmasiPass.getText().toString().isEmpty()){
            alertDialog(getResources().getString(R.string.warning_tittle),getResources().getString(R.string.warning_dataempty),RegisterActivity.this);
            return false;
        }
        return true;
    }

    public void alertDialog(String tittle, String message, Context c){
        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle(tittle);
        builder.setMessage(message);
        builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mFirebaseAuth.getCurrentUser();
    }

//    private void startPhoneNumberVerification(String phoneNumber) {
//        PhoneAuthProvider.getInstance().verifyPhoneNumber(
//                phoneNumber,        // Phone number to verify
//                60,                 // Timeout duration
//                TimeUnit.SECONDS,   // Unit of timeout
//                this,               // Activity (for callback binding)
//                mCallbacks);        // OnVerificationStateChangedCallbacks
//    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = task.getResult().getUser();
                            startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                            finish();
                        } else {
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
//                                mVerificationField.setError("Invalid code.");
                            }
                        }
                    }
                });

    }


}
