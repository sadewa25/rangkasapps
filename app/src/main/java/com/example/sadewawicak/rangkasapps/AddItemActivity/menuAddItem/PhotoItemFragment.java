package com.example.sadewawicak.rangkasapps.AddItemActivity.menuAddItem;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.example.sadewawicak.rangkasapps.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoItemFragment extends Fragment {


    public PhotoItemFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.photo_item_surface)SurfaceView preview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_photo_item, container, false);
        ButterKnife.bind(this,rootView);

        return rootView;
    }

}
