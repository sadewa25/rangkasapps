package com.example.sadewawicak.rangkasapps.BerandaActivity.adapterBeranda;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sadewawicak.rangkasapps.R;

import java.util.List;


/**
 * Created by androiddev on 12/02/2018.
 */

public class adapterDataBeranda extends RecyclerView.Adapter<adapterDataBeranda.MyViewHolder>{

    private List<modelBerandaItem> dataModels;
    private Context mContext;
    private String status;

    public adapterDataBeranda(List<modelBerandaItem> dataReports, Context mContext){
        this.dataModels=dataReports;
        this.mContext = mContext;
    }

    @Override
    public adapterDataBeranda.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_menu_beranda, parent, false);

        return new adapterDataBeranda.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(adapterDataBeranda.MyViewHolder holder, int position) {
        final modelBerandaItem model = dataModels.get(position);
        holder.image.setImageResource(model.getImg());
        holder.textTittle.setText(""+model.getTextTittle());
        holder.textMessage.setText(""+model.getTextMessage());
    }

    @Override
    public int getItemCount() {
        return dataModels.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView textTittle,textMessage;
        private ImageView image;

        public MyViewHolder(View view) {
            super(view);
            textTittle = (TextView)view.findViewById(R.id.beranda_item_recycler_texttittle);
            textMessage = (TextView)view.findViewById(R.id.beranda_item_recycler_textmessage);
            image = (ImageView)view.findViewById(R.id.beranda_item_recycler_img);
        }
    }

}
