package com.example.sadewawicak.rangkasapps.BerandaActivity.adapterBeranda;

/**
 * Created by sadewawicak on 1/8/18.
 */

public class modelBeranda {

    private String nameMenu;
    private int imgMenu;

    public String getNameMenu() {
        return nameMenu;
    }

    public int getImgMenu() {
        return imgMenu;
    }

    public modelBeranda(String nameMenu, int imgMenu){
        this.imgMenu = imgMenu;
        this.nameMenu = nameMenu;
    }
}
