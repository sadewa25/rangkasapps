package com.example.sadewawicak.rangkasapps.BerandaActivity;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.example.sadewawicak.rangkasapps.BerandaActivity.adapterBeranda.adapterBerandaMenu;
import com.example.sadewawicak.rangkasapps.BerandaActivity.adapterBeranda.modelBeranda;
import com.example.sadewawicak.rangkasapps.BerandaActivity.adapterBeranda.adapterDataBeranda;
import com.example.sadewawicak.rangkasapps.BerandaActivity.adapterBeranda.modelBerandaItem;
import com.example.sadewawicak.rangkasapps.BerandaActivity.interfaceBeranda.InterfaceBeranda;
import com.example.sadewawicak.rangkasapps.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.DrawableBanner;
import ss.com.bannerslider.views.BannerSlider;

/**
 * Beranda Menu Apps
 */
public class BerandaFragment extends Fragment
        implements InterfaceBeranda{


    public BerandaFragment newInstance() {
        // Required empty public constructor
        return new BerandaFragment();
    }

    private adapterDataBeranda adapterDataBeranda;
    private List<modelBerandaItem> dataItemBeranda=new ArrayList<>();
    @BindView(R.id.beranda_barter_recycler)RecyclerView berandaRecycler;
    @BindView(R.id.beranda_jual_recycler)RecyclerView jualRecycler;
    @BindView(R.id.beranda_pinjam_recycler)RecyclerView pinjamRecycler;
    @BindView(R.id.beranda_donasi_recycler)RecyclerView donasiRecycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_beranda, container, false);

        ButterKnife.bind(this,rootView);

        generateBanner(rootView);
        generateMenu(rootView);

        generateDataBarter();
        generateDataSell();
        generateDataBorrow();
        generateDataDonations();

        return rootView;
    }

    @Override
    public void generateMenu(View rootView) {
        GridView gridView = (GridView)rootView.findViewById(R.id.grid_menu_beranda);
        List<modelBeranda> dataMenu = new ArrayList<>();
        dataMenu.add(new modelBeranda(getResources().getString(R.string.menu_apps_barter),R.drawable.handshake));
        dataMenu.add(new modelBeranda(getResources().getString(R.string.menu_apps_sell),R.drawable.dollar));
        dataMenu.add(new modelBeranda(getResources().getString(R.string.menu_apps_borrow),R.drawable.transfer));
        dataMenu.add(new modelBeranda(getResources().getString(R.string.menu_apps_donation),R.drawable.gift));
        adapterBerandaMenu adapter = new adapterBerandaMenu(getContext(),dataMenu);
        gridView.setAdapter(adapter);
    }

    @Override
    public void generateBanner(View rootView) {
        BannerSlider mBannerSlider = (BannerSlider)rootView.findViewById(R.id.slider_beranda);
        List<Banner> dataSlider = new ArrayList<>();
        dataSlider.add(new DrawableBanner(R.drawable.promo_1));
        dataSlider.add(new DrawableBanner(R.drawable.promo_2));
        dataSlider.add(new DrawableBanner(R.drawable.promo_3));
        mBannerSlider.setBanners(dataSlider);
    }

    @Override
    public void generateDataBarter() {
        for (int i = 0;i<20;i++){
            dataItemBeranda.add(new modelBerandaItem("SAMSUNG GALAXY NOTE",R.drawable.temp_item_img,"FINISH"));
        }
        adapterDataBeranda = new adapterDataBeranda(dataItemBeranda,getContext());
        berandaRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
        berandaRecycler.setAdapter(adapterDataBeranda);
    }

    @Override
    public void generateDataSell() {
        for (int i = 0;i<20;i++){
            dataItemBeranda.add(new modelBerandaItem("SAMSUNG GALAXY NOTE",R.drawable.temp_item_img,"FINISH"));
        }
        adapterDataBeranda = new adapterDataBeranda(dataItemBeranda,getContext());
        jualRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
        jualRecycler.setAdapter(adapterDataBeranda);
    }

    @Override
    public void generateDataBorrow() {
        for (int i = 0;i<20;i++){
            dataItemBeranda.add(new modelBerandaItem("SAMSUNG GALAXY NOTE",R.drawable.temp_item_img,"FINISH"));
        }
        adapterDataBeranda = new adapterDataBeranda(dataItemBeranda,getContext());
        pinjamRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
        pinjamRecycler.setAdapter(adapterDataBeranda);
    }

    @Override
    public void generateDataDonations() {
        for (int i = 0;i<20;i++){
            dataItemBeranda.add(new modelBerandaItem("SAMSUNG GALAXY NOTE",R.drawable.temp_item_img,"FINISH"));
        }
        adapterDataBeranda = new adapterDataBeranda(dataItemBeranda,getContext());
        donasiRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
        donasiRecycler.setAdapter(adapterDataBeranda);
    }

}
