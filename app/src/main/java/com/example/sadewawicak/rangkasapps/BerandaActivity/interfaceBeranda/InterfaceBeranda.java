package com.example.sadewawicak.rangkasapps.BerandaActivity.interfaceBeranda;

import android.content.Context;
import android.view.View;

/**
 * Created by androiddev on 02/03/2018.
 */

public interface InterfaceBeranda {

    void generateMenu(View rootView);
    void generateBanner(View rootView);

    void generateDataBarter();
    void generateDataSell();
    void generateDataBorrow();
    void generateDataDonations();

}
