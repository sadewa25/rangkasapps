package com.example.sadewawicak.rangkasapps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends Activity {

    private FirebaseAuth mFirebaseAuth;
    @BindView(R.id.signin_email)EditText signinEmail;
    @BindView(R.id.signin_password)EditText signinPassword;

    @OnClick(R.id.signin_register)void toRegister(){
        startActivity(new Intent(SignInActivity.this,RegisterActivity.class));
    }
    @OnClick(R.id.signin_login)void toMainActivity(){
        if (validationEmpty() == false){
            mFirebaseAuth.signInWithEmailAndPassword(signinEmail.getText().toString(),
                    signinPassword.getText().toString()).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){

                        Toast.makeText(SignInActivity.this, "User : "+mFirebaseAuth.getCurrentUser(), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        finish();
                    }else{
                        Toast.makeText(SignInActivity.this, "GAGAL LOGIN", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private boolean validationEmpty(){
        if (signinEmail.getText().toString().isEmpty()||signinPassword.getText().toString().isEmpty()){
            RegisterActivity ra = new RegisterActivity();
            ra.alertDialog(getResources().getString(R.string.warning_tittle),getResources().getString(R.string.warning_dataempty),SignInActivity.this);
            return true;
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        ButterKnife.bind(this);
        mFirebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mFirebaseAuth.getCurrentUser()!=null){
            startActivity(new Intent(SignInActivity.this,MainActivity.class));
        }
    }



}
