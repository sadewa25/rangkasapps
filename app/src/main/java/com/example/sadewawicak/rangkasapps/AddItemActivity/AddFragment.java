package com.example.sadewawicak.rangkasapps.AddItemActivity;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sadewawicak.rangkasapps.AddItemActivity.menuAddItem.GalleryItemFragment;
import com.example.sadewawicak.rangkasapps.AddItemActivity.menuAddItem.PhotoItemFragment;
import com.example.sadewawicak.rangkasapps.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddFragment extends Fragment {


    public AddFragment newInstance() {
        // Required empty public constructor
        return new AddFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rooView = inflater.inflate(R.layout.fragment_add, container, false);
        ButterKnife.bind(this,rooView);

        startActivity(new Intent(getContext(),AddItemActivityDialog.class));

        return rooView;
    }

}
