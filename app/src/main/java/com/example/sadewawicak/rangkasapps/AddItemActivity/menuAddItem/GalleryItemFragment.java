package com.example.sadewawicak.rangkasapps.AddItemActivity.menuAddItem;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sadewawicak.rangkasapps.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;
import static android.widget.ImageView.ScaleType.CENTER_CROP;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryItemFragment extends Fragment
        implements InterfaceGalleryItem, AdapterView.OnItemClickListener{


    public GalleryItemFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.gallery_item_img)ImageView imageView;
    @BindView(R.id.gallery_item_grid)GridView gridView;

    private Cursor cc = null;
    private static Uri[] mUrls = null;
    private static String[] strUrls = null;
    private String[] mNames = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gallery_item, container, false);
        ButterKnife.bind(this,rootView);

        getDataImage();

        gridView.setOnItemClickListener(this);

        return rootView;
    }

    @Override
    public void getDataImage() {
        // It have to be matched with the directory in SDCard
        cc = getContext().getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null,
                null);

        // File[] files=f.listFiles();
        if (cc != null) {

            new Thread() {
                public void run() {
                    try {
                        cc.moveToFirst();
                        mUrls = new Uri[cc.getCount()];
                        strUrls = new String[cc.getCount()];
                        mNames = new String[cc.getCount()];
                        for (int i = 0; i < cc.getCount(); i++) {
                            cc.moveToPosition(i);
                            mUrls[i] = Uri.parse(cc.getString(1));
                            strUrls[i] = cc.getString(1);
                            mNames[i] = cc.getString(3);
                            //Log.e("mNames[i]",mNames[i]+":"+cc.getColumnCount()+ " : " +cc.getString(3));
                        }

                    } catch (Exception e) {
                    }
                }
            }.start();

            gridView.setAdapter(new ImageAdapter(getContext()));

        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Bitmap bmp = decodeURI(mUrls[i].getPath());
        imageView.setImageBitmap(bmp);
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;
        public ImageAdapter(Context c) {
            this.mContext = c;
        }
        public int getCount() {
            return cc.getCount();
        }
        public Object getItem(int position) {
            return null;
        }
        public long getItemId(int position) {
            return 0;
        }
        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.gallery_item, null);
            try {
                ImageView imageView = (ImageView) v.findViewById(R.id.gallery_item_imgcustom);
                Bitmap bmp = decodeURI(mUrls[position].getPath());
                Bitmap resize = Bitmap.createScaledBitmap(bmp,120,200,true);
                imageView.setImageBitmap(resize);
            } catch (Exception e) {
                Log.d(TAG, "getView: "+e.toString());
            }
            return v;
        }
    }
    public Bitmap decodeURI(String filePath){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Only scale if we need to
        // (16384 buffer for img processing)
        Boolean scaleByHeight = Math.abs(options.outHeight - 100) >= Math.abs(options.outWidth - 100);
        if(options.outHeight * options.outWidth * 2 >= 16384){
            // Load, scaling to smallest power of 2 that'll get it <= desired dimensions
            double sampleSize = scaleByHeight
                    ? options.outHeight / 100
                    : options.outWidth / 100;
            options.inSampleSize =
                    (int)Math.pow(2d, Math.floor(
                            Math.log(sampleSize)/Math.log(2d)));
        }

        // Do the actual decoding
        options.inJustDecodeBounds = false;
        options.inTempStorage = new byte[512];
        Bitmap output = BitmapFactory.decodeFile(filePath, options);

        return output;
    }

}
