package com.example.sadewawicak.rangkasapps.BerandaActivity.adapterBeranda;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sadewawicak.rangkasapps.R;

import java.util.List;

/**
 * Created by sadewawicak on 1/13/18.
 */

public class adapterBerandaMenu extends BaseAdapter {

    private List<modelBeranda> dataMenu;
    private Context c;

    public adapterBerandaMenu(Context c,List<modelBeranda> dataMenu){
        this.c = c;
        this.dataMenu = dataMenu;
    }

    @Override
    public int getCount() {
        return dataMenu.size();
    }

    @Override
    public Object getItem(int i) {
        return dataMenu.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null){
            view = LayoutInflater.from(c).inflate(R.layout.beranda_menu,viewGroup,false);
        }
        modelBeranda data = (modelBeranda)getItem(i);
        ImageView imgMenu = (ImageView)view.findViewById(R.id.adapter_beranda_img);
        imgMenu.setImageResource(data.getImgMenu());
        TextView nameMenu = (TextView)view.findViewById(R.id.adapter_beranda_text);
        nameMenu.setText(data.getNameMenu());

        return view;


    }
}

